package soapserver

import grails.gorm.transactions.Transactional
//import org.grails.cxf.javax.GrailsCxfEndpoint

import javax.jws.WebMethod
import javax.jws.WebResult




@Transactional
//@GrailsCxfEndpoint
class DemoService {

    @WebMethod
    String demoMethod() {
        return "demo"
    }
}